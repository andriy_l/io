package com.noname.learning;

import com.noname.learning.models.Pupil;
import com.noname.learning.models.Teacher;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Teacher teacher = new Teacher(5);
        teacher.setName("Andriy");
        Pupil pupil = new Pupil();
        pupil.setName("Petro");
        // association
        teacher.teach(pupil);
    }
}
