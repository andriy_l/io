package com.noname.learning.repository;

import com.noname.learning.models.Pupil;

public interface PupilRepository {
    long save(Pupil pupil);
    Pupil getPupil(long id);
    long update(Pupil pupil);
    long update(long id, Pupil pupil);
    void remove(Pupil pupil);
}
