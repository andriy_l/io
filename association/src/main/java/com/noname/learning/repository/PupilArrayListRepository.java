package com.noname.learning.repository;

import com.noname.learning.models.Pupil;

import java.util.ArrayList;
import java.util.List;

public class PupilArrayListRepository implements PupilRepository {
    private final List<Pupil> pupilList = new ArrayList<Pupil>();

    public long save(Pupil pupil) {
        pupilList.add(pupil);
        return pupilList.indexOf(pupil);
    }

    public Pupil getPupil(long id) {
        return pupilList.get((int)id);
    }

    public long update(Pupil pupil) {
        Pupil tmpPupil = pupilList.get(pupilList.indexOf(pupil));
        tmpPupil.setAge(pupil.getAge());
        return save(pupil);
    }

    public long update(long id, Pupil pupil) {
        pupilList.set((int)id, pupil);
        return id;
    }

    public void remove(Pupil pupil) {
        pupilList.remove(pupil);
    }
}
