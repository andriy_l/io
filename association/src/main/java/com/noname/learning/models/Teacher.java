package com.noname.learning.models;

import java.util.ArrayList;
import java.util.List;

public class Teacher extends Person {
    private final List<Subject> subjects = new ArrayList<Subject>();
    private final int qualification;

    private SpecialCourse freeClasses = new SpecialCourse();

    public Teacher(int qualification) {
        this.qualification = qualification;
    }

    public int getQualification() {
        return qualification;
    }



    public List<Subject> getSubjects() {

        return subjects;
    }

    public void teach(Pupil pupil) {
        System.out.println("Teaching "+ pupil.getName());
    }

    public void teach(List<Pupil> pupils) {
        System.out.println("Teaching " + pupils);
    }


}
