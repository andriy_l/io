package com.noname.learning.models;

import java.io.Serializable;
import java.util.List;

public class Pupil extends Person implements Serializable {
    private static final long serialVersionUID = 3073298640381431147L;


    private List<Subject> subjectList;

    public List<Subject> getSubjectList() {
        return subjectList;
    }

    public void setSubjectList(List<Subject> subjectList) {
        this.subjectList = subjectList;
    }

    public Pupil() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pupil)) return false;
        if (!super.equals(o)) return false;

        Pupil pupil = (Pupil) o;

        return getSubjectList() != null ? getSubjectList().equals(pupil.getSubjectList()) : pupil.getSubjectList() == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (getSubjectList() != null ? getSubjectList().hashCode() : 0);
        return result;
    }
}
