package com.noname.learning.models;

import java.util.List;

public class StudyClass {

   // composition
   private StudyClassMonitor studyClassMonitor;
   private List<Pupil> pupils;

    public StudyClass(Teacher teacher) {
        this.studyClassMonitor = new StudyClassMonitor(teacher);
    }

}
