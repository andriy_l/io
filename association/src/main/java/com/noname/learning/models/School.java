package com.noname.learning.models;

import java.util.ArrayList;
import java.util.List;

public class School {
    // aggregation
    private final List<Teacher> teachers = new ArrayList<Teacher>();
    private final List<Pupil> pupils = new ArrayList<Pupil>();
}
